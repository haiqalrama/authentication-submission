const db = require("../db");
const bcrypt = require("bcrypt");

function rootRequest(req, res) {
  res.render("homepage.ejs");
}

function loginPage(req, res) {
  if (req.isAuthenticated()) {
    if (req.user.role === "admin") {
      return res.redirect("/admin");
    } else {
      return res.redirect("/student");
    }
  } else {
    res.render("login.ejs", { error: req.session.messages });
  }
}

async function login(req, res) {
  const user = req.user;
  if (user.role === "admin") {
    return res.redirect("/admin");
  } else {
    return res.redirect("/student");
  }
}

async function adminPage(req, res) {
  if (req.isAuthenticated()) {
    if (req.user.role === "admin") {
      const id = req.user.id;
      const result = await db.query(
        `SELECT * FROM users WHERE role = 'student'`
      );
      const result2 = await db.query(
        `SELECT * FROM users WHERE role = 'admin' AND id=${id}`
      );
      const user = result2.rows[0];
      const users = result.rows;
      res.render("admin.ejs", { users, user });
    } else {
      res.render("limited_access.ejs");
    }
  } else {
    res.render("404.ejs");
  }
}

async function studentPage(req, res) {
  if (req.isAuthenticated()) {
    const id = req.user.id;
    const result = await db.query(`SELECT * FROM users WHERE id = ${id}`);
    const user = result.rows[0];
    res.render("student.ejs", { user });
  } else {
    res.render("404.ejs");
  }
}

function logout(req, res) {
  req.logout();
  res.redirect("/");
}

function registerPage(req, res) {
  if (req.isAuthenticated()) {
    if (req.user.role === "admin") {
      return res.redirect("/admin");
    } else {
      return res.redirect("/student");
    }
  } else {
    res.render("register.ejs");
  }
}

async function register(req, res) {
  const { email, password, name, address } = req.body;
  const hashPassword = await bcrypt.hash(password, 10);
  const user = await db.query(
    `INSERT INTO users (email, password, role, address, name) VALUES ('${email}', '${hashPassword}', 'student', '${address}', '${name}') RETURNING *`
  );
  if (user.rowCount === 1) {
    res.redirect("/login");
  } else {
    res.redirect("/register");
  }
}

async function deleteStudent(req, res) {
  const id = req.params.id;
  await db.query(`DELETE FROM users WHERE id = ${id}`);
  res.redirect("/admin");
}

module.exports = {
  rootRequest,
  login,
  adminPage,
  studentPage,
  logout,
  loginPage,
  registerPage,
  register,
  deleteStudent,
};
